##########################################################################################
# Author: Zahoor
##########################################################################################
# Some source code has been used from a Github and referenced.
# Appropriate accreditation has been mentioned throughout the code.
# Name: Propositional Logic Clause Parser
# Author: Marko Manninen
# Source: https://github.com/markomanninen/PLCParser/blob/master/pyPLCParser/PLCParser.py
# License: MIT
#########################################################################################

# -*- coding: utf-8 -*-
# Importing Libraries
import re
import numpy as np
import pandas as pd
from prettytable import PrettyTable
from tabulate import tabulate
import random
from PyQt5.QtWidgets import QFileDialog, QMainWindow, QApplication
import sys
import os
import math


# PyQt Window class
class Window(QMainWindow):
    # Implement save file dialog
    def save_file(
            # Save document as a LaTeX file (.tex)
            self):
        x = QFileDialog.getSaveFileName(self, "Save Files", "", "LaTex Files (*.tex)")[0]
        assert x # Makes sure x is not an empty string
        return x # Returns the filename


# Create a PyQt App
app = QApplication(sys.argv)
WINDOW = Window()


# https://github.com/markomanninen/PLCParser/blob/master/pyPLCParser/PLCParser.py
# XOR function
def xor(a):
    # Filter out number of trues in an array. Input array has 2 truth values eg [True, False]
    # If number of true's is odd (not divisible by 2) then return True otherwise return False
    return len(list(filter(lambda x: x, a))) % 2 != 0


# Implies function to do logical implies function
def implies(a):
    # De Morgan's Law: p → q ≡ ¬p ∨ q
    # Calculating ¬p
    a[0] = OPERATORS[NOT]['func'](a[0])  # 'func' comes from OPERATORS dictionary, see line 53
    # Returning  ¬p ∨ q
    return OPERATORS[OR]['func'](a)

# https://github.com/markomanninen/PLCParser/blob/master/pyPLCParser/PLCParser.py
# Operator precedence e.g. NOT will be evaluated before AND etc...
NOT = -1
AND = -2
XOR = -3 # https://en.wikipedia.org/wiki/Order_of_operations#Programming_languages
OR = -4
IMPL = -5
BICON = -6

# https://github.com/markomanninen/PLCParser/blob/master/pyPLCParser/PLCParser.py
# https://en.wikipedia.org/wiki/List_of_logic_symbols
OPERATORS = {
    # https://en.wikipedia.org/wiki/Negation
    NOT: {'word': 'not', 'math': '¬', 'func': lambda x: not x},                 # not for NOT operator as not 1 = 0
    # https://en.wikipedia.org/wiki/Logical_conjunction
    AND: {'word': 'and', 'math': '∧', 'func': all},                             # and for AND operator as and([1,0]) = 0
    # https://en.wikipedia.org/wiki/Logical_disjunction
    OR:  {'word': 'or', 'math': '∨', 'func': any},                              # or for OR operator as or([1,0]) = 1
    # https://en.wikipedia.org/wiki/Exclusive_or
    XOR: {'word': 'xor', 'math': '⊕', 'func': xor},                            # xor function defined above for XOR operator
    # https://en.wikipedia.org/wiki/Material_conditional
    IMPL: {'word': 'implies', 'math': '→', 'func': implies},                    # implies function defined above for IMPLIES operator
    # https://en.wikipedia.org/wiki/Logical_biconditional
    BICON: {'word': 'biconditional', 'math': '↔', 'func': lambda a: not xor(a)} # not of xor for BICON operator
}

# https://github.com/markomanninen/PLCParser/blob/master/pyPLCParser/PLCParser.py
# Defining Booleans and symbols for true and false
B_False = 0
B_True = 1
BOOLEANS = {
    B_True: {'word': 'true', 'math': '⊤'},
    B_False: {'word': 'false', 'math': '⊥'}
}

# https://github.com/markomanninen/PLCParser/blob/master/pyPLCParser/PLCParser.py
# True can be represented both as true and ⊤
TRUES = [BOOLEANS[B_True]['word'], BOOLEANS[B_True]['math']]

# https://github.com/markomanninen/PLCParser/blob/master/pyPLCParser/PLCParser.py
# Define parenthesis, double and single quotations and precedence rule. -6 will be evaluated last
paren = ['(', ')']

# https://github.com/markomanninen/PLCParser/blob/master/pyPLCParser/PLCParser.py
# string_wrapper used to store both quotes that can be used to wrap string
string_wrapper = ["'", '"']         # Assign parenthesis, double and single quotations
precedence = (-6, -5, -4, -3, -2, -1)
open_paren, close_paren = paren
STRING_LITERALS = re.compile('|'.join([r"%s[^%s\\]*(?:\\.[^%s\\]*)*%s" % (w, w, w, w) for w in string_wrapper]))
flag = False

# https://www.overleaf.com/learn
# Declaring global variables to store LaTeX syntax
# LaTeX header including packages
latex_document_header = r'''\documentclass[12 pt]{extreport}
\usepackage{enumitem}
\usepackage{graphics}
\usepackage{xcolor}
\usepackage[margin=1.2in]{geometry}
\usepackage{lmodern}
\usepackage{array}
\usepackage {tikz}

\usepackage{atbegshi,picture}

\def\mydate{\leavevmode\hbox{\the\day-\twodigits\month-\twodigits\year}}
\def\twodigits#1{\ifnum#1<10 0\fi\the#1}

\AtBeginShipout{\AtBeginShipoutUpperLeft{%
  \put(\dimexpr\paperwidth-1cm\relax,-1.5cm){\makebox[0pt][r]{\framebox{\mydate}}}%
}}

\renewcommand{\arraystretch}{1.3}

\newcolumntype{P}[1]{>{\centering\arraybackslash}p{#1}}
\begin{document}
'''

# Starting/ending enumeration
latex_begin_enumerate = r'''
\begin{enumerate}
'''
latex_end_enumerate = r'''
\end{enumerate}
'''

# Variable for Space
latex_newline = r'''
'''

# Adding Title in Blue Color
latex_blue_large_title = r'''\thispagestyle{plain}
\begin{center}
\LARGE
\textcolor{blue}{\textbf{'''

latex_title = r'''}}
\vspace{0.2cm}
\end{center}
'''

# Adding 0.2cm Space between Tables
latex_vspace = r'''
\vspace*{0.2cm}
'''

# Centering the tables
latex_begin_center = r'''
\begin{center}
'''
latex_end_center = r'''
\end{center}
'''

# Multiple choice options
latex_begin_choices = r'''
\begin{choices}
'''
latex_end_choices = r'''
\end{choices}
'''
latex_end_line = r'''
\\
'''
latex_newpage = r'''
\newpage
'''


# Adding Footer
latex_document_footer = latex_end_enumerate + latex_end_enumerate + r'''\end{document}'''

latex_empty_box = r'''\framebox(14,12){} '''

# Different question statements for different documents
truth_table_question_statement_latex = r'''\item { Complete the truth table for the following proposition }''' + latex_begin_enumerate
proposition_checker_question_statement_latex = r'''\item { Construct the truth table below for the proposition to determine whether it is a tautology, contingency or contradiction, tick the correct box }''' + latex_begin_enumerate
merge_option_truth_table_question_statement_latex = r'''\item { Complete the truth table for the following proposition }''' + latex_begin_enumerate
merge_option_proposition_checker_question_statement_latex = latex_end_enumerate + proposition_checker_question_statement_latex
merge_option_proposition_checker_answer_statement_latex = latex_end_enumerate + r'''\item''' + latex_begin_enumerate
select_proposition_type_boxes_latex = r'''\framebox(14,12){} \enspace A. Tautology \qquad\framebox(14,12){} \enspace B. Contingency \qquad \framebox(14,12){} \enspace C. Contradiction'''
blank_5pt_latex = r'''\hspace{5pt}'''     #


# If exception occurred, pass by returning none
class ExceptionOccurred(Exception):
    pass


# https://github.com/markomanninen/PLCParser/blob/master/pyPLCParser/PLCParser.py
# Alternative forms for booleans and operators are accepted. Verifies if the input operator corresponds to any of operations allowed by this program
# returns the boolean value or the operator if found in our pre-defined dictionaries named OPERATORS and BOOLEANS
def verify_boolean_and_operator(input_operator_or_boolean_value):
    stripped_lower_input_operator_or_boolean_value = input_operator_or_boolean_value.strip().lower()
    # Checks if the input is an operator by going through the operators dictionary element by element
    for op, d in OPERATORS.items():
        if stripped_lower_input_operator_or_boolean_value == d['word'] or stripped_lower_input_operator_or_boolean_value == d['math']:  # Operator can be inputted as word or math symbol
            return op
    # Checks if the input is a Boolean value by going through the booleans dictionary element by element
    for op, d in BOOLEANS.items():
        if stripped_lower_input_operator_or_boolean_value == d['word'] or stripped_lower_input_operator_or_boolean_value == d['math']:  # Booleans can be inputted as word or as '⊤' or '⊥'
            return op
    return input_operator_or_boolean_value


# https://github.com/markomanninen/PLCParser/blob/master/pyPLCParser/PLCParser.py
# This function deformats a parsed string from numbers to either words or symbols
def deformat_string(lst, operator_type="word"):
    try:
        def rec(current_item, operator_type, first=True):
            if not isinstance(current_item, list):              # Check if current item is in list or not
                current_item_lower = str(current_item).lower()  # If not, convert it to lower case
                for op, d in BOOLEANS.items():
                    if current_item_lower == d['word'] or current_item_lower == d['math']:        # If current item is a boolean, return it as it is
                        return current_item
                current_item = current_item.replace(string_wrapper[0], '\\' + string_wrapper[
                    0])  # Otherwise insert single quotations around it
                return string_wrapper[0] + current_item + string_wrapper[0]
            result = []
            if not first:
                result.append(open_paren)  # If input argument first is false, insert open parentheses at start
            for item in current_item:
                if not isinstance(item, list) and item in OPERATORS:  # Check if current item is not in list and it is an operator
                    result.append(OPERATORS[item][operator_type])     # Then replace operator by its given type and append it to a
                else:
                    result.append(rec(item, operator_type, False))
            if not first:
                result.append(close_paren)  # Insert closing parenthesis at end.
            return ' '.join(map(str, result))

        # https://github.com/markomanninen/PLCParser/blob/master/pyPLCParser/PLCParser.py
        # Recursively calls itself, it finds & replaces text terms with operators so that they can be computed by other functions
        return rec(lst, operator_type)
    except Exception as ex:
        print(ex)
        return None


# https://github.com/markomanninen/PLCParser/blob/master/pyPLCParser/PLCParser.py
# This function parses the operator
def parse_single(literals, literals_stack, operators, unary_operator):
    if len(literals_stack) < 1:   # If length of stack is less than 1
        raise ExceptionOccurred("ill-formed string: multiple operators or length 0.")
    if len(literals_stack) == 1:  # If length of stack is 1.
        if literals_stack[0] != unary_operator and not literals_stack[0] in operators:     # If first element is not a unary operator and not an operator
            if not isinstance(literals_stack[0], list) and literals_stack[0] in literals:  # And if it is not a list
                literals_stack[0] = literals[literals_stack[0]]  # Replace ith element of stack with element of literals at index equal to value of ith element of stack
                # Loops through string wrapper elements that are quotes e.g single and double quote
                for w in string_wrapper:
                    literals_stack[0] = literals_stack[0].replace("\\%s" % w, w)       # Remove \\ and spaces
            return literals_stack[0]
        else:
            raise ExceptionOccurred(
                "ill-formed string: Operator not found: %s." % literals_stack[0])     # Raise an exception
    if len(operators) > 0:  # If there is one or more operators
        operator = operators[0]
        try:
            position = len(literals_stack) - 1 - literals_stack[::-1].index(operator)  # Find position of operator
            return [parse_single(literals, literals_stack[:position], operators, unary_operator), operator,
            parse_single(literals, literals_stack[position + 1:], operators[1:], unary_operator)]
        except ValueError:  # Raise truth value error
            return parse_single(literals, literals_stack, operators[1:], unary_operator)
    if literals_stack[0] != unary_operator:
        raise ExceptionOccurred("ill-formed input. Operator expected, %s found." % literals_stack[0])
    return [unary_operator, parse_single(literals, literals_stack[1:], operators, unary_operator)]


# https://github.com/markomanninen/PLCParser/blob/master/pyPLCParser/PLCParser.py
# Receives a proposition expression entered by user and after verification returns a stack containing operators(e.g. for and, or, implies) code and variables
def parse_string(input_string):
    try:
        input_string = input_string.strip()         # Leading whitespaces are removed
        literals = {}                               # Declare a dictionary for string literals
        lit = STRING_LITERALS.search(input_string)  # Search for parts of string in double quotation marks.
        literals_count = 1
        # Below loop is used to count number of literals in the input string (proposition expression) by going through all the string
        while lit:
            literal_group = lit.group()
            key = "L%s" % literals_count
            literals[key] = literal_group[1:-1]
            input_string = input_string.replace(literal_group, " %s " % key)
            lit = STRING_LITERALS.search(input_string)
            literals_count += 1
        input_string = input_string.replace(open_paren, ' %s ' % open_paren)    # Add extra space before open parenthesis
        input_string = input_string.replace(close_paren, ' %s ' % close_paren)  # Add extra space before closing parenthesis

        for operator, options in OPERATORS.items():
            input_string = input_string.replace(options['math'],
                                                ' %s ' % options['math'])  # Replace operators according to option
        literal_string = input_string

        parsed_string = ' '.join(literal_string.split()).split(' ')        # Split literal_string by space
        parsed_string = [open_paren] + parsed_string + [close_paren]       # Insert extra open and closing parenthesis
        parsed_string = map(verify_boolean_and_operator,
                            parsed_string)   # Find alternative form of operators and unary operators
        parsed_string = list(parsed_string)
        stack = [[]]
        for item in parsed_string:
            if item == open_paren:
                stack[-1].append([])
                stack.append(stack[-1][-1])  # Append [] to stack for every open parenthesis
            elif item == close_paren:        # If closing parenthesis found, pop last element of stack
                stack.pop()
                stack[-1][-1] = parse_single(literals, stack[-1][-1], precedence[:-1],
                                             precedence[-1])        # Call of function

                if not stack:
                    raise ExceptionOccurred(
                        'Malformed input. Parenthesis mismatch. Opening parenthesis missing.')  # Error dialogue
            else:
                stack[-1].append(item)
        if len(stack) > 1:
            raise ExceptionOccurred(
                'Malformed input. Parenthesis mismatch. Closing parenthesis missing.')  # If length of stack at the end is more
                # than 1 it means one or more closing parenthesis are missing. Or alternatively, there's more than one parenthesis
        return stack.pop()
    except ExceptionOccurred as pe:
        return None  # Return None if exception occurred


# https://github.com/markomanninen/PLCParser/blob/master/pyPLCParser/PLCParser.py
# appends result of an operator in an existing table received as "table"
# operator is received with operands in "current_item"
# returns truth_table after appended column
def out_value(current_item, table, negate=True):
    if not isinstance(current_item, list):
        if table and current_item in table:
            current_item = table[
                current_item]  # If not a list; it means it is a variable. Extract truth value from table
        return (str(current_item).lower() in TRUES) != flag  # Convert variable name to lower case
    result_truth_table = []
    operator = AND
    for item in current_item:
        if not isinstance(item, list) and item in OPERATORS:
            if item == NOT:
                negate = False
            else:
                operator = item
        else:
            result_truth_table.append(out_value(item, table))  # Append truth value calculated to table
    return OPERATORS[operator]['func'](
        result_truth_table) == negate  # Compare value with input argument negate, if it is True return True; otherwise return False


# https://github.com/markomanninen/PLCParser/blob/master/pyPLCParser/PLCParser.py
# This function evaluates a logical expression given table of truth values of variables
def evaluate_string(i, table={}):
    try:
        return out_value(parse_string(i) if type(i) == str else i, table)
    except ExceptionOccurred as pe:
        return None


#########################################################################################################################################
# This function separates variables and propositions from a parsed expression. Parsed expression is a nested list with upto depth 5. For
# example, parsed string for expression "p and not q or not (r implies s) and (r implies s)" is
# [[['p', -2, [-1, 'q']], -4, [[-1, ['r', -5, 's']], -2, ['r', -5, 's']]]]
# These nested lists are separated into simple lists and variables to print out intermediate truth table values.
########################################################################################################################################
def propositions_and_variables(out):
    variables= []
    propositions = []
    for i in range(len(out)): # Going in depth 1 and checking whether it contains another list or not
        if np.array(out[i]).size > 1: # If length is greater than 1, it is another list otherwise some variable
            for j in range(len(out[i])):  # Going in depth 2 and checking whether it contains another list or not
                if (np.array(
                        out[i][j]).size > 1):  # If length is greater than 1 it is another list otherwise some variable
                    for k in range(
                            len(out[i][j])):  # Going in depth 3 and checking whether it contains another list or not
                        if (np.array(out[i][j][
                                         k]).size > 1):   # If length is greater than 1 it is another list otherwise some variable

                            for l in range(len(out[i][j][
                                                   k])):       # Going in depth 4 and checking whether it contains another list or not
                                if (np.array(out[i][j][k][
                                                 l]).size > 1): # If length is greater than 1 it is another list otherwise some variable
                                    for m in range(len(out[i][j][k][
                                                           l])):       # Going in depth 5 and checking whether it contains another list or not
                                        if (np.array(out[i][j][k][l][
                                                         m]).size > 1): # If length is greater than 1 it is another list otherwise some variable
                                            propositions.append(deformat_string(out[i][j][k][l][m], operator_type="math")) # Deformatting inner most
                                            # list at depth 5 to a string so that it is in terms of special logical symbols.
                                            # For example ['r', 5, 's'] will be deformated as 'r → s'. Then appending it to our list of propositions.

                                        if (isinstance(out[i][j][k][l][m],
                                                       str)):       # Checking if an element at depth 5 is a single string literal
                                            variables.append(
                                                out[i][j][k][l][m]) # If it is, append it to our list of variables.


                                    propositions.append(deformat_string(out[i][j][k][l], operator_type="math")) # Deformatting list at depth 4 to a string so that it is in terms of special logical
                                                                                                                # symbols and appending it to list of propositions.
                                if (isinstance(out[i][j][k][l],
                                              str)): # Checking if an element at depth 4 is a single string literal
                                    variables.append(out[i][j][k][l]) # If it is, append it to our list of variables.
                            propositions.append(deformat_string(out[i][j][k], operator_type="math")) # Deformatting list at depth 3 to a string so that it is in terms of special logical symbols
                                                                                                     # and appending it to list of propositions.

                        if(isinstance(out[i][j][k],str)):  # Checking if an element at depth 3 is a single string literal
                            variables.append(out[i][j][k]) # If it is, append it to our list of variables.
                    propositions.append(deformat_string(out[i][j], operator_type="math"))   # Deformatting list at depth 2 to a string so that it is in terms of special logical symbols
                                                                                            # and append it to list of propositions.

                if(isinstance(out[i][j],str)):  # Checking if an element at depth 2 is a single string literal
                    variables.append(out[i][j]) # If it is, append it to our list of variables.
            propositions.append(deformat_string(out[i], operator_type="math"))  # Deformatting list at depth 1 to a string so that it is in terms of special logical symbols
                                                                                # and appending it to list of propositions.
        if(isinstance(out[i],str)):  # Checking if an element at depth 1 is a single string literal
            variables.append(out[i]) # If it is, append it to our list of variables.
    propositions.append(deformat_string(out, operator_type="math")) # Deformatting complete list to a string so that it is in terms of special logical symbols
                                                                    # and appending it to list of propositions.
    return variables,propositions # Returning list of variables and propositions


# This functions removes brackets around a not operator.
def remove_not_brackets(string):
    brackets_count = string.count('(¬')  # Count number of brackets to remove
    try:
        for bracket_index in range(brackets_count):
            ind = string.index('(¬')     # Search for index of opening bracket to remove

            for j in range(ind + 2, len(string)):  # Start a loop from index of opening bracket
                if string[j] == ')':               # Search for corresponding closing bracket and remove it
                    string = string[:j] + string[j + 1:]
                    break
    except Exception as ex:
        print(ex)
        pass
    string = string.replace('(¬', '¬')  # Remove all opening brackets
    return string


# This function generates pandas dataframe (table) of truth values given a logical expression.
def truth_table(expression):
    out = parse_string(expression)  # Parsing input expression to nested lists.
    variables, propositions = propositions_and_variables(
        out)                        # Extracting propositions and variables from this nested list.

    variables = sorted(set(variables))  # If a valid ordered variable
    length = len(variables)
    if length == 1:    # If there is only one variable, initiate dataframe with 1 column.
        if variables[0] not in ['p', 'q', 'r', 's']:
            print('variable', variables[0], 'not supported.')
        else:
            df = pd.DataFrame({variables[0]: [True, False]})

    if length == 2:    # If there are 2 variables, initiate dataframe with 2 columns named as p and q.
        df = pd.DataFrame({variables[0]: [True, True, False, False], variables[1]: [True, False, True, False]})
    elif length == 3:  # If there are 3 variables, initiate dataframe with 3 columns named as p, q and r.
        df = pd.DataFrame({variables[0]: [True, True, True, True, False, False, False, False],
                           variables[1]: [True, True, False, False, True, True, False, False],
                           variables[2]: [True, False, True, False, True, False, True, False]})
    elif length == 4:  # If there are 4 variables, initiate dataframe with 4 columns named as p, q, r and s.
        df = pd.DataFrame({variables[0]: [True, True, True, True, True, True, True, True,
                                          False, False, False, False, False, False, False, False],
                           variables[1]: [True, True, True, True, False, False, False, False,
                                          True, True, True, True, False, False, False, False],
                           variables[2]: [True, True, False, False, True, True, False, False,
                                          True, True, False, False, True, True, False, False],
                           variables[3]: [True, False, True, False, True, False, True, False,
                                          True, False, True, False, True, False, True, False]})
    propositions = propositions[:-1]

    my_dict = df.to_dict('records')  # Convert dataframe to a dictionary
    answers = np.ones((len(my_dict), len(propositions)),
                     dtype=bool)     # Make a two dimensional array with as many columns as there are propositions
    # and as many rows as there are truth values.
    for i in range(len(my_dict)):
        for j in range(len(propositions)):
            answers[i][j] = evaluate_string(propositions[j], my_dict[
                i])  # Evaluate each proposition for each row of truth value. It will give either True or False

    for k in range(len(propositions)):
        df[propositions[k]] = answers[:, k]  # Append 2-d of answers as columns to our dataframe.
    df = df.replace(False, 'F')              # Replace False with F in dataframe
    df = df.replace(True, 'T')               # Replace True with T

    columns = list(df.columns)
    for i in range(len(columns)):
        columns[i] = columns[i].replace("'", '')      # Remove single quotation from column names
        columns[i] = columns[i].replace(" ", '')      # Remove spaces from column names
        columns[i] = remove_not_brackets(columns[i])  # Remove brackets around not operator
    df.columns = columns
    return df


# This function takes a dataframe which contains complete truth table of any expression and returns the type of expression
# e.g. tautology, contradiction or contingency
def get_proposition_type(complete_truth_table_dataframe):
    last_column = complete_truth_table_dataframe.iloc[:, -1].values

    # Check if last column all values are T then it is a tautology
    result = all(cell_value == 'T' for cell_value in last_column)

    if result:
        propositions = 'tautology'
    else:
        result = all(cell_value == 'F' for cell_value in last_column)
        if result:
            propositions = 'contradiction'
        else:
            propositions = 'contingency'
    return propositions


# This function takes a dataframe which contains complete truth table of any expression
# Returns a dataframe with randomly unfilled cells
def get_partially_completed_dataframe(complete_truth_table_dataframe, total_variables_count):
    partial_dataframe = complete_truth_table_dataframe.copy()  # Partially complete complete dataframe
    # Identify how many columns need to be included where empty boxes are to be put so that user will have to fill them
    column_in_which_value_need_to_change = total_variables_count
    partial_dataframe_to_change = complete_truth_table_dataframe.iloc[:, column_in_which_value_need_to_change:]
    partial_dataframe_to_change = partial_dataframe_to_change.copy()
    more_than_one_column = True if len(partial_dataframe_to_change.columns) > 1 else False

    # Iterate through the dataframe and make random blanks in proposition question version to be filled by user
    # e.g. for proposition x or y the below code will replace few truth values with blanks in column (x or y)
    for index, j in enumerate(partial_dataframe_to_change.columns):
        if more_than_one_column:
            if index == 0:
                if partial_dataframe.shape[0] == 2:
                    index_to_remove_in_column = random.choice([0, 1])
                    partial_dataframe_to_change.loc[
                        index_to_remove_in_column, j] = latex_empty_box
                    partial_dataframe[j] = partial_dataframe_to_change[j].values
                else:
                    index_to_remove_in_column = np.random.choice(list(range(partial_dataframe_to_change.shape[0] - 1)),
                                                                 1, replace=False)
                    partial_dataframe_to_change.loc[
                        index_to_remove_in_column, j] = latex_empty_box
                    partial_dataframe[j] = partial_dataframe_to_change[j].values
            else:
                if partial_dataframe.shape[0] == 2:
                    index_to_remove_in_column = random.choice([0, 1])
                    partial_dataframe_to_change.loc[
                        index_to_remove_in_column, j] = latex_empty_box
                    partial_dataframe[j] = partial_dataframe_to_change[j].values
                else:
                    index_to_remove_in_column = np.random.choice(list(range(partial_dataframe_to_change.shape[0] - 1)),
                                                                 2, replace=False)
                    index_to_remove_in_column = index_to_remove_in_column.flatten()
                    partial_dataframe_to_change.loc[
                        index_to_remove_in_column, j] = latex_empty_box
                    partial_dataframe[j] = partial_dataframe_to_change[j].values
        else:
            index_to_remove_in_column = np.random.choice(list(range(partial_dataframe_to_change.shape[0] - 1)), 2)
            index_to_remove_in_column = index_to_remove_in_column.flatten()
            partial_dataframe_to_change.loc[index_to_remove_in_column, j] = latex_empty_box
            partial_dataframe[j] = partial_dataframe_to_change[j].values
    return partial_dataframe


# This function takes a dataframe which contains complete truth table of any expression
# Returns a dataframe with all truth values as empty other than variables column
def get_incomplete_dataframe(complete_truth_table_dataframe):
    # Copy the same dataframe to create incomplete truth table
    i_df = complete_truth_table_dataframe.copy()
    # Replace True and False for incomplete with space
    i_df[i_df.columns[-1]] = i_df[i_df.columns[-1]].str.replace('T', ' ').replace('F', ' ')
    return i_df


# This function takes a dataframe which contains complete truth table of any expression and prints truth table on console
# Returns nothing
def print_truth_table_on_console(complete_truth_table_dataframe):
    print()

    # Pretty Table part
    truth_values_pretty_table = PrettyTable()
    truth_values_pretty_table.field_names = complete_truth_table_dataframe.columns
    for z in range(complete_truth_table_dataframe.shape[0]):
        truth_values_pretty_table.add_row(complete_truth_table_dataframe.iloc[z])
    print(truth_values_pretty_table)
    print()


# This function takes a dataframe which contains complete truth table of any expression
# Returns nothing and also prints what type of propositional formula it is.
def print_truth_table_type_on_console(complete_truth_table_dataframe):
    proposition_type = get_proposition_type(complete_truth_table_dataframe)
    print('The propositional logic expression is a {}'.format(proposition_type))
    print()


# Takes an input expression and generates LaTeX words with italic expression and a space on both sides of a binary operator
def make_variables_italic_and_add_space_with_operators(input_expression):
    output_latex = r'''''' + input_expression
    output_latex = output_latex.replace('p', r'''\it{p}''')
    output_latex = output_latex.replace('q', r'''\it{q}''')
    output_latex = output_latex.replace('r', r'''\it{r}''')
    output_latex = output_latex.replace('s', r'''\it{s}''')
    output_latex = output_latex.replace('∧', blank_5pt_latex + '∧' + blank_5pt_latex)
    output_latex = output_latex.replace('∨', blank_5pt_latex + '∨' + blank_5pt_latex)
    output_latex = output_latex.replace('⊕', blank_5pt_latex + '⊕' + blank_5pt_latex)
    output_latex = output_latex.replace('→', blank_5pt_latex + '→' + blank_5pt_latex)
    output_latex = output_latex.replace('↔', blank_5pt_latex + '↔' + blank_5pt_latex)
    return output_latex


# This function generates the main truth table into LaTeX for truth table answer version from the dataframe received in arguments
def generate_latex_for_truth_table_answer_version(complete_truth_table_dataframe, truth_table_answer_main_latex_string):
    global latex_newline
    global latex_begin_center
    global latex_end_center
    global latex_vspace

    # Tabulate the complete dataframe in LaTeX form and store in table
    truth_table_answer_table = tabulate(complete_truth_table_dataframe, headers='keys', tablefmt='latex_raw',
                                       showindex=False, stralign="center")
    # Convert the LaTeX string table to raw string
    truth_table_answer_raw_table = r'{}'.format(truth_table_answer_table)
    # Replace the columns in the table with static length columns for dynamic table sizes for larger expressions
    spl_new = truth_table_answer_raw_table.split('\n')[0].split('{')[-1].replace('}', '')
    var = truth_table_answer_raw_table.split('\n')[2][:-2].replace('&', '').split()
    count_chars = 0
    for x in range(len(var)):
        count_chars = count_chars + len(var[x])
    temp = ''
    for y in range(len(var)):
        width = round(len(var[y]) * (13.6 / count_chars), 1)
        temp = temp + '|P{' + str(width) + 'cm}'
    temp = temp + '|'
    truth_table_answer_raw_table = truth_table_answer_raw_table.replace(spl_new, temp)
    # Separate expression to be numbered
    truth_table_answer_stri = complete_truth_table_dataframe.columns[-1]
    # Number the expression in LaTeX format raw string
    truth_table_answer_chap = r'\item { ' + truth_table_answer_stri + '}'
    # Combine main (that contains table, first iteration it will be empty), chap (that contains numbered expression) and sp (that contains space)
    truth_table_answer_main_latex_string = truth_table_answer_main_latex_string + truth_table_answer_chap + latex_newline
    # Start center after the numbered heading
    truth_table_answer_main_latex_string = truth_table_answer_main_latex_string + latex_begin_center
    # Add table after centering
    truth_table_answer_main_latex_string = truth_table_answer_main_latex_string + truth_table_answer_raw_table
    # End Centering
    truth_table_answer_main_latex_string = truth_table_answer_main_latex_string + latex_end_center
    return truth_table_answer_main_latex_string


# This function generates main truth table into LaTeX for the truth table question version from the dataframe received in arguments
def generate_latex_for_truth_table_question_version(complete_truth_table_dataframe,
                                                    truth_table_question_main_latex_string):
    global latex_newline
    global latex_begin_center
    global latex_end_center
    global latex_vspace

    truth_table_question_dataframe = get_incomplete_dataframe(complete_truth_table_dataframe)
    # Tabulate the incomplete dataframe in LaTeX form and store into the table
    truth_table_question_table = tabulate(truth_table_question_dataframe, headers='keys', tablefmt='latex_raw',
                                          showindex=False, stralign="center")
    # Convert the LaTeX string table to raw string
    truth_table_question_raw_table = r'{}'.format(truth_table_question_table)
    # Replace the columns in the table with static length columns for dynamic table sizes for larger expressions
    spl_new = truth_table_question_raw_table.split('\n')[0].split('{')[-1].replace('}', '')
    var = truth_table_question_raw_table.split('\n')[2][:-2].replace('&', '').split()
    count_chars = 0
    for x in range(len(var)):
        count_chars = count_chars + len(var[x])  # count number of columns
    temp = ''
    for y in range(len(var)):
        width = round(len(var[y]) * (13.6 / count_chars), 1)  # Dynamic column width. Larger the expression, more the column width is.
        temp = temp + '|P{' + str(width) + 'cm}'              # Setting column width in centimeters.
    temp = temp + '|'
    truth_table_question_raw_table = truth_table_question_raw_table.replace(spl_new, temp)
    # Rest is the same as truth_table_answer version
    truth_table_question_stri = truth_table_question_dataframe.columns[-1]
    truth_table_question_chap = r'\item { ' + truth_table_question_stri + '}'
    truth_table_question_main_latex_string = truth_table_question_main_latex_string + truth_table_question_chap + latex_newline
    truth_table_question_main_latex_string = truth_table_question_main_latex_string + latex_begin_center
    truth_table_question_main_latex_string = truth_table_question_main_latex_string + truth_table_question_raw_table
    truth_table_question_main_latex_string = truth_table_question_main_latex_string + latex_end_center
    truth_table_question_main_latex_string = truth_table_question_main_latex_string + latex_vspace
    return truth_table_question_main_latex_string


# This function generates the main truth table in LaTeX for proposition checker question version from the dataframe received in arguments
def generate_latex_for_proposition_checker_question_version(complete_truth_table_dataframe,
                                                            proposition_checker_question_main_latex_string,
                                                            proposition_variables_count):
    global latex_newline
    global latex_begin_center
    global latex_end_center
    global latex_vspace
    global latex_empty_box
    global select_proposition_type_boxes_latex

    proposition_checker_question_dataframe = complete_truth_table_dataframe.copy()
    # Get total columns as a list from dataframe
    columns = list(complete_truth_table_dataframe)
    columns_counter = 1
    for column in columns:
        # Add empty boxes against columns other than the variable columns
        if columns_counter > proposition_variables_count:
            proposition_checker_question_dataframe[column] = proposition_checker_question_dataframe[column].str.replace(
                'T', latex_empty_box).replace('F', latex_empty_box)
        columns_counter += 1

    # Tabulate the incomplete dataframe in LaTeX form and store into table
    proposition_checker_question_table = tabulate(proposition_checker_question_dataframe, headers='keys',
                                                  tablefmt='latex_raw', showindex=False, stralign="center")
    # Convert the LaTeX string table to raw string
    proposition_checker_question_table = r'{}'.format(proposition_checker_question_table)
    # Replace the columns in the table with static length columns for dynamic table sizes for larger expressions
    spl_new = proposition_checker_question_table.split('\n')[0].split('{')[-1].replace('}', '')
    var = proposition_checker_question_table.split('\n')[2][:-2].replace('&', '').split()
    count_chars = 0
    for x in range(len(var)):
        count_chars = count_chars + len(var[x])  # Count number of columns
    temp = ''
    for y in range(len(var)):
        width = round(len(var[y]) * (13.6 / count_chars),
                      1)  # Dynamic column width. Larger the expression, more the column width is.
        temp = temp + '|P{' + str(width) + 'cm}'              # Setting column width in centimeters.
    temp = temp + '|'
    proposition_checker_question_table = proposition_checker_question_table.replace(spl_new, temp)
    # Rest is the same as truth_table_answer version
    proposition_checker_question_stri = proposition_checker_question_dataframe.columns[-1]
    proposition_checker_question_chap = r'\item { ' + proposition_checker_question_stri + '}'
    proposition_checker_question_main_latex_string = proposition_checker_question_main_latex_string + proposition_checker_question_chap + latex_newline
    proposition_checker_question_main_latex_string = proposition_checker_question_main_latex_string + latex_begin_center
    proposition_checker_question_main_latex_string = proposition_checker_question_main_latex_string + proposition_checker_question_table
    proposition_checker_question_main_latex_string = proposition_checker_question_main_latex_string + latex_end_center
    # Adding proposition type selection options
    proposition_checker_question_main_latex_string = proposition_checker_question_main_latex_string + select_proposition_type_boxes_latex
    proposition_checker_question_main_latex_string = proposition_checker_question_main_latex_string + latex_vspace
    return proposition_checker_question_main_latex_string


# This function generates the main truth table LaTeX for proposition checker answer version from the dataframe received in arguments
def generate_latex_for_proposition_checker_answer_version(complete_truth_table_dataframe,
                                                          proposition_checker_answer_main_latex_string):
    global latex_newline
    global latex_begin_center
    global latex_end_center
    global latex_vspace

    # Copies the original dataframe to create the proposition checker version
    proposition_type = get_proposition_type(complete_truth_table_dataframe)
    proposition_checker_answer_dataframe = complete_truth_table_dataframe.copy()
    # Tabulate the incomplete dataframe in LaTeX form and store into table
    proposition_checker_answer_table = tabulate(proposition_checker_answer_dataframe, headers='keys',
                                                tablefmt='latex_raw', showindex=False, stralign="center")
    # Convert the LaTeX string table to raw string
    proposition_checker_answer_table = r'{}'.format(proposition_checker_answer_table)
    # Replace the columns in table with static length columns for dynamic table sizes for larger expressions
    spl_new = proposition_checker_answer_table.split('\n')[0].split('{')[-1].replace('}', '')
    var = proposition_checker_answer_table.split('\n')[2][:-2].replace('&', '').split()
    count_chars = 0
    for x in range(len(var)):
        count_chars = count_chars + len(var[x])  # Count number of columns
    temp = ''
    for y in range(len(var)):
        width = round(len(var[y]) * (13.6 / count_chars),
                     1)                          # Dynamic column width. Larger the expression; more the column width is.
        temp = temp + '|P{' + str(width) + 'cm}' # Setting column width in centimeters.
    temp = temp + '|'
    proposition_checker_answer_table = proposition_checker_answer_table.replace(spl_new, temp)
    # Rest is the same as truth_table_answer version
    proposition_checker_answer_stri = proposition_checker_answer_dataframe.columns[-1]
    proposition_checker_answer_chap = r'\item { ' + proposition_checker_answer_stri + '}'
    proposition_checker_answer_main_latex_string = proposition_checker_answer_main_latex_string + proposition_checker_answer_chap + latex_newline
    proposition_checker_answer_main_latex_string = proposition_checker_answer_main_latex_string + latex_begin_center
    proposition_checker_answer_main_latex_string = proposition_checker_answer_main_latex_string + proposition_checker_answer_table
    # Statement after table to state whether expression is a tautology, contradiction or contingency and add 1 cm spacing after table
    string = 'The propositional logic expression is a {}'.format(proposition_type)
    text_to_print = r"" + "\emph{" + string + "}" + r""
    proposition_checker_answer_main_latex_string = proposition_checker_answer_main_latex_string + latex_end_line + latex_vspace
    proposition_checker_answer_main_latex_string += text_to_print
    proposition_checker_answer_main_latex_string = proposition_checker_answer_main_latex_string + latex_end_center
    return proposition_checker_answer_main_latex_string


# This function generates the main truth table into LaTeX for 'partial' (missing truth values) version from the dataframe received in arguments
def generate_latex_for_partial_question_version(complete_truth_table_dataframe, truth_table_partial_main_latex_string,
                                                proposition_variables_count):
    global latex_newline
    global latex_begin_center
    global latex_end_center
    global latex_1cm_vspace
    truth_table_partial_dataframe = get_partially_completed_dataframe(complete_truth_table_dataframe,
                                                                      proposition_variables_count)
    # Create partial table
    partial_table = tabulate(truth_table_partial_dataframe, headers='keys', tablefmt='latex_raw', showindex=False,
                             stralign="center")
    # Convert the LaTeX string table to raw string
    partial_raw_table = r'{}'.format(partial_table)
    # Replace the columns in table with static length columns for dynamic table sizes for larger expressions
    spl_new = partial_raw_table.split('\n')[0].split('{')[-1].replace('}', '')
    var = partial_raw_table.split('\n')[2][:-2].replace('&', '').split()
    count_chars = 0
    for x in range(len(var)):
        count_chars = count_chars + len(var[x])  # Count number of columns
    temp = ''
    for y in range(len(var)):
        width = round(len(var[y]) * (13.6 / count_chars),
                      1)                          # Dynamic column width. Larger the expression, more the column width is.
        temp = temp + '|P{' + str(width) + 'cm}'  # Setting column width in centimeters.
    temp = temp + '|'
    partial_raw_table = partial_raw_table.replace(spl_new, temp)
    # Rest is the same as truth_table_answer version
    partial_stri = truth_table_partial_dataframe.columns[-1]
    partial_chap = r'\item { ' + partial_stri + '}'
    truth_table_partial_main_latex_string = truth_table_partial_main_latex_string + partial_chap + latex_newline
    truth_table_partial_main_latex_string = truth_table_partial_main_latex_string + latex_begin_center
    truth_table_partial_main_latex_string = truth_table_partial_main_latex_string + partial_raw_table
    truth_table_partial_main_latex_string = truth_table_partial_main_latex_string + latex_end_center
    truth_table_partial_main_latex_string = truth_table_partial_main_latex_string + latex_vspace
    return truth_table_partial_main_latex_string


# This function takes the truth table LaTeX string, LaTeX file name, LaTeX file title
# and attaches LaTeX headers and footers while saving LaTeX in local storage with specified filename
# is_multiple_choice is used to know if we need to add LaTeX header code for multiple choice too or not
def save_latex_to_file(main_latex, latex_file_name, latex_file_title, is_multiple_choice=False):
    global latex_begin_enumerate
    global latex_document_header
    global latex_blue_large_title
    global latex_title
    global latex_document_footer

    # Reduce extra whitespaces from LaTex column
    main_latex = re.sub(r'P{([0-9.])*cm}', 'c', main_latex)

    # Add top title for incomplete table
    document_main_title = latex_file_title
    # Combine all headers, title, numbered lists, tables and footer
    final_latex_document_string = latex_document_header + latex_blue_large_title + document_main_title + latex_title
    final_latex_document_string += latex_begin_enumerate + main_latex + latex_document_footer

    # Replace the operators with the LaTeX format operators
    final_latex_document_string = final_latex_document_string.replace('∧', '$\land$').replace('∨', '$\lor$').replace(
        '¬', r'$\neg$').replace('→', r'$\rightarrow$').replace('⊕', '$\oplus$').replace('↔', '$\leftrightarrow$')

    # Save the LaTeX format in a file
    is_saved = 'overwritten' if os.path.isfile(latex_file_name) else 'saved'
    with open(latex_file_name, 'w', encoding="utf-8") as f:
        f.write(final_latex_document_string)
    f.close()
    msg = f"Latex Document {is_saved} to file " + latex_file_name
    print("")
    print("-" * len(msg))
    print(msg)
    print("-" * len(msg))
    print("")


# Main method that defines user interface and generates LaTeX according to user's selection
def __main__():
    global truth_table_question_statement_latex
    global proposition_checker_question_statement_latex
    global merge_option_truth_table_question_statement_latex
    global merge_option_proposition_checker_question_statement_latex

    # Defining main table syntax variables for complete and incomplete tables
    truth_table_answer_main_latex_string = r''''''
    truth_table_question_main_latex_string = r''''''
    truth_table_partial_main_latex_string = r''''''
    proposition_checker_question_main_latex_string = r''''''
    proposition_checker_answer_main_latex_string = r''''''
    merge_option_question_main_latex_string = r''''''
    merge_option_answer_main_latex_string = r''''''

    # Below two variables are used to know if we need to add question in the main LaTeX string
    is_truth_table_question = True
    is_proposition_checker_question = True

    # User Guide
    print('========================================================================')
    print('                      Truth Table Generator                             ')
    print('========================================================================')
    print('This tool generates truth tables for propositional formulas and allows  ')
    print('you to save them into a LaTeX document for teaching & learning purposes.')
    print('Four propositional variables are supported which are p, q, r and s.     ')
    print('Propositional logic operators can be entered as words or symbols.       ')
    print("For example 'not (p or q) implies r' or '¬ (p v q) -> r'.               ")
    print('========================================================================')
    print('Supported words and corresponding symbols are given below:              ')
    print('========================================================================')
    print('            word                                     symbol             ')
    print('========================================================================')
    print('1.          not                     |        ¬        or         ~      ')
    print('2.          and                     |        &        or         ^      ')
    print('3.          or                      |        v        or         |      ')
    print('4.          xor                     |                 ⊕                ')
    print('5.          implies                 |                 ->                ')
    print('6.          biconditional           |                 <->               ')
    print('========================================================================')

    # Below loop keeps running unless user presses an invalid input or chose to end the program
    proposition = ''

    is_saving = False
    while True:
        # Below while loop repeats until user enters a valid integer
        while not is_saving:
            try:
                # Number of propositions to input
                total_propositions_to_input = int(input("Please enter the number of propositions:\t"))
                # If number of propositions are negative or zero system ask again
                if total_propositions_to_input <= 0:
                    print('Negative or 0 is not allowed as number of propositions.')
                    continue
                # Breaks the loop if the entered value is a valid positive integer
                break
            except ValueError as ex:
                print('Please enter a valid integer.')

        # Check if number of propositions is more than one then pluralise with the letter 's'
        # as 50% propositions are in truth table portion for merge version (version 3)
        proposition_for_the_section = math.ceil(total_propositions_to_input / 2.0)
        if proposition_for_the_section > 1:
            merge_option_truth_table_question_statement_latex = merge_option_truth_table_question_statement_latex.replace("proposition ", "propositions ")
        # as 50% propositions are for proposition portion in merge version (version 3)
        proposition_for_the_section = math.floor(total_propositions_to_input / 2.0)
        if proposition_for_the_section > 1:
            merge_option_proposition_checker_question_statement_latex = merge_option_proposition_checker_question_statement_latex.replace("proposition ", "propositions ")

        # If total propositions are more than one, then make plural with letter 's' for version 1 and version 2
        if total_propositions_to_input > 1:
            truth_table_question_statement_latex = truth_table_question_statement_latex.replace("proposition ", "propositions ")
            proposition_checker_question_statement_latex = proposition_checker_question_statement_latex.replace("proposition ", "propositions ")

        # Below variable is used to store the average number of questions that will be used in option 3 (merging version 1 & 2)
        average_questions_for_all_options = math.ceil(total_propositions_to_input / 2.0)
        propositions_counter = 1  # Keeping proposition count
        while propositions_counter <= total_propositions_to_input and not is_saving:  # Loop until proposition count
            if propositions_counter == 1:
                expression = input("Please input your 1st logical expression:\t")
            elif propositions_counter == 2:
                expression = input("Please input your 2nd logical expression:\t")
            elif propositions_counter == 3:
                expression = input("Please input your 3rd logical expression:\t")
            else:
                expression = input("Please input your " + str(propositions_counter) + "th logical expression:\t")

            # Replace input operators to the ones defined in original function.
            expression = expression.replace('<->', '↔').replace('~', '¬').replace('v', '∨').replace('|', '∨').replace('V', '∨').replace('->', '→').replace('^', '∧').replace('&', '∧').replace('⊕', 'xor')
            # Parse the input expression
            parsed_expression = parse_string(expression)

            if parsed_expression is not None:  # Check if expression is parsable i.e. supported variables/operators have been used
                # Separate Variables and propositions
                variables, propositions = propositions_and_variables(parsed_expression)
                variables = sorted(set(variables))

                op = ['not', '¬', 'and', '∧', 'or', '∨', 'xor', 'implies', '→', 'biconditional', '↔']

                # Split input expression on spaces
                splitted = np.array(expression.split())
                # Check if the length of splitted input remains same if operators are removed i.e no operators are added
                if len(np.setdiff1d(splitted, op)) == len(splitted):
                    print('\nInvalid expression. One or more variables or operators are missing. Please try again.\n')
                # Check whether the variables are p, q, r and s for 1, 2, 3 and 4 variable propositions respectively
                elif len(variables) == 2 and (variables[0] not in ['p', 'q', 'r', 's'] or variables[1] not in ['p', 'q', 'r', 's']):
                    print('\nInvalid variable. For 2 variable formula, supported variables are p, q, r and s. Please try again.\n')
                elif len(variables) == 3 and (
                        variables[0] not in ['p', 'q', 'r', 's'] or variables[1] not in ['p', 'q', 'r', 's'] or
                        variables[2] not in ['p', 'q', 'r', 's']):
                    print('\nInvalid variable. For 3 variable formula, supported variables are p, q, r and s. Please try again.\n')
                elif len(variables) == 4 and (
                        variables[0] not in ['p', 'q', 'r', 's'] or variables[1] not in ['p', 'q', 'r', 's'] or
                        variables[2] not in ['p', 'q', 'r', 's'] or variables[3] not in ['p', 'q', 'r', 's']):
                    print('\nInvalid variable. For 4 variable formula, supported variables are p, q, r and s. Please try again.\n')
                # If everything is fine go here
                else:
                    try:
                        # Create Dataframe of the expression's Truth Table
                        complete_truth_table_dataframe = truth_table(expression)
                        print_truth_table_on_console(complete_truth_table_dataframe)
                        print_truth_table_type_on_console(complete_truth_table_dataframe)

                        # Changing truth table headers and making variables italic and adding space with binary operators
                        complete_truth_table_dataframe.columns = [make_variables_italic_and_add_space_with_operators(col_name) for col_name in complete_truth_table_dataframe.columns]
                        # Calling function to know the proposition type
                        propositions = get_proposition_type(complete_truth_table_dataframe)
                        total_variable = len(variables)

                        partial_truth_table_dataframe = get_partially_completed_dataframe(
                            complete_truth_table_dataframe, total_variable)

                        # Generating main truth tables into LaTeX for all of the available formats
                        truth_table_answer_main_latex_string = generate_latex_for_truth_table_answer_version(
                            complete_truth_table_dataframe, truth_table_answer_main_latex_string)
                        truth_table_question_main_latex_string = generate_latex_for_truth_table_question_version(
                            complete_truth_table_dataframe, truth_table_question_main_latex_string)
                        proposition_checker_question_main_latex_string = generate_latex_for_proposition_checker_question_version(
                            complete_truth_table_dataframe, proposition_checker_question_main_latex_string,
                            total_variable)
                        proposition_checker_answer_main_latex_string = generate_latex_for_proposition_checker_answer_version(
                            complete_truth_table_dataframe, proposition_checker_answer_main_latex_string)
                        truth_table_partial_main_latex_string = generate_latex_for_partial_question_version(
                            complete_truth_table_dataframe, truth_table_partial_main_latex_string, total_variable)

                        # temp question type stores the value that will identify which type of question should be generated for 3rd menu saving option
                        temp_question_type = 0
                        # Check if the divisor is positive only, then do division
                        if average_questions_for_all_options > 0:
                            if propositions_counter <= average_questions_for_all_options:
                                if propositions_counter % 2 == 0:     # checks if proposition number is even or odd to make a decision about which type of question should be chosen
                                    temp_question_type = 1  # selecting partially completed truth table question
                                else:
                                    temp_question_type = 0  # selecting truth table question
                            else:
                                temp_question_type = 2  # selecting proposition checker question
                        if temp_question_type == 0:
                            # This means the proposition is the multiple choice question
                            if is_truth_table_question:
                                # Setting boolean value to false so that the question is not added for all questions of the same type
                                is_truth_table_question = False
                                merge_option_question_main_latex_string = merge_option_question_main_latex_string + merge_option_truth_table_question_statement_latex
                            merge_option_question_main_latex_string = generate_latex_for_truth_table_question_version(complete_truth_table_dataframe, merge_option_question_main_latex_string)
                            merge_option_answer_main_latex_string = generate_latex_for_truth_table_answer_version(complete_truth_table_dataframe, merge_option_answer_main_latex_string)
                        elif temp_question_type == 1:
                            # This means the proposition is the partially completed truth values, in the intermediate and last column of the truth table
                            merge_option_question_main_latex_string = generate_latex_for_partial_question_version(complete_truth_table_dataframe, merge_option_question_main_latex_string, total_variable)
                            merge_option_answer_main_latex_string = generate_latex_for_truth_table_answer_version(complete_truth_table_dataframe, merge_option_answer_main_latex_string)
                        else:
                            if is_proposition_checker_question:
                                # Setting boolean value to false so that the question is not added for all questions of same type
                                is_proposition_checker_question = False
                                merge_option_question_main_latex_string = merge_option_question_main_latex_string + merge_option_proposition_checker_question_statement_latex
                                merge_option_answer_main_latex_string = merge_option_answer_main_latex_string + merge_option_proposition_checker_answer_statement_latex
                            merge_option_question_main_latex_string = generate_latex_for_proposition_checker_question_version(complete_truth_table_dataframe, merge_option_question_main_latex_string, total_variable)
                            merge_option_answer_main_latex_string = generate_latex_for_proposition_checker_answer_version(complete_truth_table_dataframe, merge_option_answer_main_latex_string)

                        propositions_counter = propositions_counter + 1  # Increment count for number of propositions
                    except Exception as e:
                        print(e)
                        print('Invalid expression. Please try again')
            elif parsed_expression is None:  # Error dialogue
                print(
                    "\nOne or more variable(s) or operators(s) are missing or not supported.")
                print("=====================================================================")
                print("Only supported operators are:")
                print("-----------------------------")
                print("> and(^)(&)\t> or(v)(|)\n> not(~)(¬)\t> xor\n> implies(->)\t> biconditional(<->)")
                print("=====================================================================")
                print('And supported variables are:')
                print("----------------------------")
                print('p, q, r or s for any valid expression.\n\nPlease try again.\n')

        is_saving = True
        # User Options
        print("Please choose the type of questions you want to generate:")
        print("=========================================================")
        print("Press 1 for Completing the Truth Table of a Propositional Formula")
        print("Press 2 for Multiple Choice Question (Tautology/Contingency/Contradiction)")
        print("Press 3 for Merging both of the above Categories into one LaTeX Document")
        print("Press any other key to exit")
        print()

        program_version_option = input("Waiting for your input:\t")
        if program_version_option != '1' and program_version_option != '2' and program_version_option != '3':
            exit()

        print()
        print("Please choose an option:")
        print("========================")
        print("Press 1 to save the question(s) LaTeX document")
        print("Press 2 to save the answer(s) LaTeX document")
        print("Press any other key to exit")
        print()

        # Choose option
        output_version_option = input("Waiting for your input:\t")
        # End the program if user selected/pressed any key other than specified ones
        if output_version_option != '1' and output_version_option != '2':
            exit()

        # Read the destination file name
        try :
            latex_file_name = WINDOW.save_file()
        except:
            continue

        # Version 1 saves incompleted truth table for the intermediate and last column truth values as question(s)
        if program_version_option == '1' and output_version_option == '1':
            truth_table_question_main_latex_string = truth_table_question_statement_latex + truth_table_question_main_latex_string
            save_latex_to_file(main_latex=truth_table_question_main_latex_string, latex_file_name=latex_file_name,
                               latex_file_title='Truth Table Questions')
            continue
        if program_version_option == '1' and output_version_option == '2': # Completed truth table solution
            truth_table_answer_main_latex_string = r'''\item''' + latex_begin_enumerate + truth_table_answer_main_latex_string
            save_latex_to_file(main_latex=truth_table_answer_main_latex_string, latex_file_name=latex_file_name,
                               latex_file_title='Truth Table Solutions')
            continue
        # Version 2 generates and saves a multiple choice question for the truth table
        if program_version_option == '2' and output_version_option == '1':
            proposition_checker_question_main_latex_string = proposition_checker_question_statement_latex + proposition_checker_question_main_latex_string
            save_latex_to_file(main_latex=proposition_checker_question_main_latex_string,
                               latex_file_name=latex_file_name, latex_file_title='Truth Table Questions')
            continue
        if program_version_option == '2' and output_version_option == '2':
            proposition_checker_answer_main_latex_string = r'''\item''' + latex_begin_enumerate  + proposition_checker_answer_main_latex_string
            save_latex_to_file(main_latex=proposition_checker_answer_main_latex_string, latex_file_name=latex_file_name,
                               latex_file_title='Truth Table Solutions')
            continue
        # Version 3 saves the document by merging version 1 & 2, with an addition to random truth values that need to be completed
        if program_version_option == '3' and output_version_option == '1':
            save_latex_to_file(main_latex=merge_option_question_main_latex_string,
                               latex_file_name=latex_file_name, latex_file_title='Truth Table Questions')
            continue
        if program_version_option == '3' and output_version_option == '2':
            merge_option_answer_main_latex_string = r'''\item''' + latex_begin_enumerate + merge_option_answer_main_latex_string
            save_latex_to_file(main_latex=merge_option_answer_main_latex_string, latex_file_name=latex_file_name,
                               latex_file_title='Truth Table Solutions')
            continue
        else:
            break

# Call the main function to start the execution
__main__()
sys.exit(app.exec_())
