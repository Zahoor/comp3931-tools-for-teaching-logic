import sys
from contextlib import redirect_stdout
import pandas as pd
import Truth_Table_Gen
import io
from unittest import TestCase


class Test(TestCase):

    def test_print_truth_table_type_on_console_positive(self):
        """
            This test should capture output produced to console
        """
        captured_output = io.StringIO()
        with redirect_stdout(captured_output):
            data = [['T', 'T', 'T'], ['T', 'F', 'T'], ['F', 'F', 'F']]
            dataframe_for_p_or_q = pd.DataFrame(data=data, columns=['p', 'q', 'p?q'])
            Truth_Table_Gen.print_truth_table_type_on_console(dataframe_for_p_or_q)  # Call unchanged function.
            output = captured_output.getvalue().strip()
            assert ('The propositional logic expression is a contingency' == output)

        captured_output.close()
        captured_output = io.StringIO()
        with redirect_stdout(captured_output):
            data = [['T', 'T', 'T'], ['T', 'T', 'T'], ['T', 'T', 'T']]
            dataframe_for_p_or_q = pd.DataFrame(data=data, columns=['p', 'q', 'p?q'])
            Truth_Table_Gen.print_truth_table_type_on_console(dataframe_for_p_or_q)  # Call unchanged function.
            output = captured_output.getvalue().strip()
            assert ('The propositional logic expression is a tautology' == output)
        captured_output.close()

        captured_output.close()
        captured_output = io.StringIO()
        with redirect_stdout(captured_output):
            data = [['F', 'F', 'F'], ['F', 'F', 'F'], ['F', 'F', 'F']]
            dataframe_for_p_or_q = pd.DataFrame(data=data, columns=['p', 'q', 'p?q'])
            Truth_Table_Gen.print_truth_table_type_on_console(dataframe_for_p_or_q)  # Call unchanged function.
            output = captured_output.getvalue().strip()
            assert ('The propositional logic expression is a contradiction' == output)

    def test_print_truth_table_type_on_console_negative(self):
        """
            This test should capture no output produced to console
        """
        try:
            captured_output = io.StringIO()
            with redirect_stdout(captured_output):
                Truth_Table_Gen.print_truth_table_type_on_console("")  # Call unchanged function.
                output = captured_output.getvalue().strip()
                assert False
        except Exception:
                pass

        try:
            captured_output = io.StringIO()
            with redirect_stdout(captured_output):
                Truth_Table_Gen.print_truth_table_type_on_console("random text")  # Call unchanged function.
                output = captured_output.getvalue().strip()
                assert False
        except Exception:
                assert True

    def test_get_proposition_type_positive(self):
        """
                This test should produce the correct proposition type given a data frame
        """
        data = [['T', 'T', 'T'], ['T', 'F', 'T'], ['F', 'F', 'F']]
        dataframe_for_p_or_q = pd.DataFrame(data=data, columns=['p', 'q', 'p?q'])
        output = Truth_Table_Gen.get_proposition_type(dataframe_for_p_or_q)  # Call unchanged function.

        assert ('contingency' == output)

        data = [['T', 'T', 'T'], ['T', 'T', 'T'], ['T', 'T', 'T']]
        dataframe_for_p_or_q = pd.DataFrame(data=data, columns=['p', 'q', 'p?q'])
        output = Truth_Table_Gen.get_proposition_type(dataframe_for_p_or_q)  # Call unchanged function.
        assert ('tautology' == output)

        data = [['F', 'F', 'F'], ['F', 'F', 'F'], ['F', 'F', 'F']]
        dataframe_for_p_or_q = pd.DataFrame(data=data, columns=['p', 'q', 'p?q'])
        output = Truth_Table_Gen.get_proposition_type(dataframe_for_p_or_q)  # Call unchanged function.
        assert ('contradiction' == output)

    def test_get_proposition_type_negative(self):
        """
            This test should not produce the correct proposition type. Instead they should throw exception indicating
            correctness.
        """
        try:
            output = Truth_Table_Gen.get_proposition_type("")  # Call unchanged function.
        except Exception:
            pass

        try:
            output = Truth_Table_Gen.get_proposition_type("Random Text")  # Call unchanged function.
            assert False
        except Exception:
            assert True

    def test_xor_positive(self):
        """
            This test should produce the correct boolean operator xor result.
        """
        assert Truth_Table_Gen.xor([True, False, True]) == False
        assert Truth_Table_Gen.xor([True, True, True]) == True

    def test_xor_negative(self):
        """
            This test should not produce the correct boolean operator xor result. Instead they should throw exception
            indicating correctness.
        """
        try:
            output = Truth_Table_Gen.xor(["",""])
            assert  False
        except Exception:
            pass

        try:
            output = Truth_Table_Gen.xor([None,""])
            assert False
        except Exception:
            assert True

    def test_parse_string_positive(self):
        """
            This test should produce the correct parsed string given a proposition.
        """
        output = Truth_Table_Gen.parse_string('p and q')
        assert (output == [['p',-2,'q']])
        output = Truth_Table_Gen.parse_string('p and (q or ( p and q))')
        assert (output == [['p',-2,['q',-4,['p',-2,'q']]]])

    def test_parse_string_negative(self):
        """
            This test should not produce the correct parsed string given a wrong proposition.
        """
        output = Truth_Table_Gen.parse_string('p xsd q')
        assert output == None
        output = Truth_Table_Gen.parse_string('p and (q xcv ( p and q)')
        assert output == None

    def test_remove_not_brackets_positive(self):
        """
                This test should produce the correct output string after removing brackets around not operator.
        """
        output = Truth_Table_Gen.remove_not_brackets('p v (¬q)')
        assert output.strip() == 'p v ¬q'
        output = Truth_Table_Gen.remove_not_brackets('p v (p ^ q (p ^ (¬q)))')
        assert output.strip() == 'p v (p ^ q (p ^ ¬q))'

    def test_remove_not_brackets_negative(self):
        """
            This test should not produce the string after removing brackets around not operator.
        """
        output = Truth_Table_Gen.remove_not_brackets('p v (¬q))')
        assert output.strip() != 'p v ¬q'
        output = Truth_Table_Gen.remove_not_brackets('p v (p ^ q (p ^ ¬(¬q))))')
        assert output.strip() != 'p v (p ^ q (p ^ ¬q))'

    def test_verify_boolean_and_operator_positive(self):
        """
            This test should produce the correct int representing the boolean value or operator.
        """
        output = Truth_Table_Gen.verify_boolean_and_operator('or')
        assert (output == -4)
        output = Truth_Table_Gen.verify_boolean_and_operator('false')
        assert (output == 0)
        output = Truth_Table_Gen.verify_boolean_and_operator('xor')
        assert (output == -3)

    def test_verify_boolean_and_operator_negative(self):
        """
            This test should not produce the correct int representing the boolean value or operator given wrong input.
        """
        output = Truth_Table_Gen.verify_boolean_and_operator('ord')
        assert (output == 'ord')
        output = Truth_Table_Gen.verify_boolean_and_operator('Random Text')
        assert (output == 'Random Text')

    def test_deformat_string_positive(self):
        """
            This test should produce the correct string representing the proposition.
        """
        output = Truth_Table_Gen.deformat_string([['p',-2,'q']])
        assert output == '( \'p\' and \'q\' )'
        output = Truth_Table_Gen.deformat_string([['p',-2,['q',-4,['p',-2,'q']]]])
        assert output == '( \'p\' and ( \'q\' or ( \'p\' and \'q\' ) ) )'

    def test_deformat_string_negative(self):
        """
            This test should not produce the correct string representing the proposition given a wrong parsed string.
        """
        output = Truth_Table_Gen.deformat_string([['p', -9, 'q']])
        assert output == None
        output = Truth_Table_Gen.deformat_string([['p',-2,['q',-4,['p',-123,'q']]]])
        assert output == None

    def test_out_value_positive(self):
        """
            This test should produce the correct boolean value given the proposition.
        """
        output = Truth_Table_Gen.out_value([['p', -4, 'q']],{'p': True, 'q': False},True)
        assert output == True
        output = Truth_Table_Gen.out_value([['p', -2, 'q']],{'p': True, 'q': False},True)
        assert output == False
        output = Truth_Table_Gen.out_value([['p', -9, 'q']],{'p': True, 'q': False},True)
        assert output == False

    def test_out_value_negative(self):
        """
            This test should not produce the correct boolean value given the wrong proposition.
        """
        output = Truth_Table_Gen.out_value([['p', -9, 'q']], {'p': True, 'q': False}, True)
        assert output == False
        output = Truth_Table_Gen.out_value([['p', -9, 'q']], {'p': '', 'q': ''}, True)
        assert output == False

    def test_evaluate_string_positive(self):
        """
            This test should produce the correct boolean value given the deformatted_String.
        """
        output = Truth_Table_Gen.evaluate_string('( \'p\' and ( \'q\' or ( \'p\' and \'q\' ) ) )',{'p':True, 'q':False})
        assert output == False
        output = Truth_Table_Gen.evaluate_string('( \'p\' and \'q\' )',{'p':True, 'q':False})
        assert output == False
        output = Truth_Table_Gen.evaluate_string('( \'p\' and \'q\' )',{'p':True, 'q':True})
        assert output == True

    def test_evaluate_string_negative(self):
        """
             This test should  not produce the correct boolean value given the wrong deformatted_String.
        """
        output = Truth_Table_Gen.evaluate_string('( \'p\' xml ( \'q\' or ( \'p\' and \'q\' ) ) )',
                                                       {'p': True, 'q': False})
        assert output == False
        output = Truth_Table_Gen.evaluate_string('( \'p\' xml ( \'q\' random ( \'p\' and \'q\' ) ) )',
                                                       {'p': '', 'q': ''})
        assert output == False

    def test_propositions_and_variables_positive(self):
        """
            This test should produce the correct boolean value given the deformatted_String.
        """
        output = Truth_Table_Gen.propositions_and_variables([['p',-2,'q']])
        assert output == (['p', 'q'], ["'p' ∧ 'q'", "( 'p' ∧ 'q' )"])
        output = Truth_Table_Gen.propositions_and_variables([['p',-4,'q']])
        assert output == (['p', 'q'], ["'p' ∨ 'q'", "( 'p' ∨ 'q' )"])

    def test_propositions_and_variables_negative(self):
        """
            This test should not produce the correct boolean value given the wrong deformatted_String.
        """
        output = Truth_Table_Gen.propositions_and_variables([['p', -9, 'q']])
        assert output == (['p', 'q'], [None, None])
        output = Truth_Table_Gen.propositions_and_variables([['p', 'random operator', 'q']])
        assert output != (['p', 'q'], [None, None])

    def test_truth_table_positive(self):
        """
            This test should produce the correct Truth Table given the proposition.
        """
        output = Truth_Table_Gen.truth_table('p and q')
        output = output.iloc[:,-1]
        output = [x.strip() for x in output]
        assert output == ['T','F','F','F']
        output = Truth_Table_Gen.truth_table('p or q')
        output = output.iloc[:,-1]
        output = [x.strip() for x in output]
        assert output == ['T','T','T','F']

    def test_truth_table_negative(self):
        """
            This test should not produce the correct Truth Table given the wrong proposition. Instead must throw
            an exception.
        """
        try:
            output = Truth_Table_Gen.truth_table('p random operator q')
            assert False
        except Exception:
            pass

        try:
            output = Truth_Table_Gen.truth_table('p q')
            assert False
        except Exception:
            assert True

    def test_get_incomplete_dataframe_positive(self):
        """
                This test should produce the incomplete Truth Table given the proposition.
        """
        output = Truth_Table_Gen.get_incomplete_dataframe(Truth_Table_Gen.truth_table('p and q'))
        output = output.iloc[:,-1]
        output = [x.strip() for x in output]
        assert output == ['','','','']
        output = Truth_Table_Gen.get_incomplete_dataframe(Truth_Table_Gen.truth_table('p or q'))
        output = output.iloc[:,-1]
        output = [x.strip() for x in output]
        assert output == ['','','','']

    def test_get_incomplete_dataframe_negative(self):
        """
            This test should not produce the incomplete Truth Table given the wrong proposition. Instead must throw an
            exception.
        """
        try:
            output = Truth_Table_Gen.get_incomplete_dataframe("")
            output = output.iloc[:,-1]
            output = [x.strip() for x in output]
            assert False
        except Exception:
            pass

        try:
            data = [['T', 'T'], ['T', 'F',], ['F']]
            dataframe_for_p_or_q = pd.DataFrame(data=data, columns=['p', 'q', 'p?q'])
            output = Truth_Table_Gen.get_incomplete_dataframe(dataframe_for_p_or_q)
            output = output.iloc[:,-1]
            output = [x.strip() for x in output]
            assert output == ['','','']
        except Exception:
            assert True

    def test_get_partially_completed_dataframe_positive(self):
        """
            This test should produce the partially filled Truth Table given the proposition
        """
        output = Truth_Table_Gen.get_partially_completed_dataframe(Truth_Table_Gen.truth_table('p and q'),
                                                                         2)
        output = output.iloc[:,-1]
        output = [x.strip() for x in output]
        assert output.count('\\framebox(14,12){}') >= 1
        output = Truth_Table_Gen.get_partially_completed_dataframe(Truth_Table_Gen.truth_table('p or q'),
                                                                         2)
        output = output.iloc[:, -1]
        output = [x.strip() for x in output]
        assert output.count('\\framebox(14,12){}') >= 1

    def test_get_partially_completed_dataframe_negative(self):
        """
            This test should not produce the partial Truth Table given the wrong proposition. Instead must throw an
            exception.
        """
        try:
            output = Truth_Table_Gen.get_partially_completed_dataframe("",
                                                                             2)
            output = output.iloc[:,-1]
            output = [x.strip() for x in output]
            assert False
        except Exception:
            pass

        try:
            data = [['T', 'T'], ['T', 'F', ], ['F']]
            dataframe_for_p_or_q = pd.DataFrame(data=data, columns=['p', 'q', 'p?q'])
            output = Truth_Table_Gen.get_partially_completed_dataframe(dataframe_for_p_or_q,
                                                                             2)
            output = output.iloc[:, -1]
            output = [x.strip() for x in output]
            assert output.count('\\framebox(14,12){}') >= 1
        except Exception:
            assert True
