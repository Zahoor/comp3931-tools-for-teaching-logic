# Truth Table Generator - Tool for Teaching Logic

The software allows you to input as many propositional logic expressions and output their truth
tables including their intermediate columns. It also allows you to save the propositional formulas
and truth tables to any directory of your computer system as a LaTeX document. A question and
answer version of the document can be saved for teaching and learning purposes.

## Installation

#### This software requires Python Version 3.

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the required libraries needed to use the tool.

```bash
pip install numpy
```

```bash
pip install pandas
```

```bash
pip install tabulate
```

```bash
pip install PrettyTable
```


```bash
pip install PyQt5
```

## Terminal Font (recommended)
For Windows original command prompt:
```sh
NSimSun with a minimum of font size of 20
```
[Cmder](https://cmder.net/) is an alternative software package for Windows:
```sh
Cambria or Microsoft JhengHei UI with a minimum font size of 19
```
For Linux command prompt:
```sh
Monospace regular or Nimbus Mono PS Regular with a minimum font size of 20
```
## Usage
Locate the directory of the python code and type the command:
```sh
$ python Truth_Table_Gen.py
```

## Terminating application at any process of execution
The following command will allow you to suspend (on Linux) or terminate (on Windows)
the application when both keys are pressed together:
```sh
$ CTRL+Z
```
To continue from where you left off (only works via Linux) you can type the command:
```sh
$ fg
```

## Propositional logic features
The Software supports upto four variables which are **_p_**, **_q_**, **_r_** and **_s_**.

Supported propositional logic operators are of two types:



|        **Word**           |     **Symbols**       |
|        :---:          |      :---:        |       
|        **not**           |    '**¬**' or '**~**'     |
|        **and**           |    '**^**' or '**&**'     |
|        **or**            |    '**v**' or '**&#124;**'|
|        **xor**           |       **⊕**        |
|        **implies**       |      **->**          |
|       **biconditional**  |      **<->**         |


Inputting valid propositional logic expressions outputs the truth table.


A space is required after each variable and operator for example you can input:

- p or not p

- p v ¬p     

```
+---+----+------+
| p | ¬p | p∨¬p |
+---+----+------+
| T | F  |  T   |
| F | T  |  T   |
+---+----+------+
```

Brackets are also supported for example:

- not (p or q) implies r

- ¬(p v q) -> r

```
+---+---+---+-----+--------+----------+
| p | q | r | p∨q | ¬(p∨q) | ¬(p∨q)→r |
+---+---+---+-----+--------+----------+
| T | T | T |  T  |   F    |    T     |
| T | T | F |  T  |   F    |    T     |
| T | F | T |  T  |   F    |    T     |
| T | F | F |  T  |   F    |    T     |
| F | T | T |  T  |   F    |    T     |
| F | T | F |  T  |   F    |    T     |
| F | F | T |  F  |   T    |    T     |
| F | F | F |  F  |   T    |    F     |
+---+---+---+-----+--------+----------+
```

###### Notes:

- A space is not required for the **not** operator after inputting an open parenthesis.
- A space is also not required when inputting a **variable** after using an open parenthesis or an **operator** after a closed parenthesis.

## Saving options


There are three types of saving options providing two documents for each respectively:

1. Construction of Propositional Formula and Truth Table (Version 1)



|      Question(s) Document        |      Answer(s) Document       |
|       :---:                      |      :---:                    |       
|   The LaTeX document requires you to complete the intermediate and last column of the truth table based on the number of generated propositional expressions.           | LaTeX document outputs the solution to completed truth table(s).



2. Multiple Choices (Tautology/Contradiction/Contingency) (Version 2)


|        Question(s) Document           |     Answer(s) Document       |
|       :---:                           |           :---:              |       
|       The LaTeX document is in the form of a multiple choice environment, for example random truth table entries from the intermediate and last column would need to be completed.            |    LaTeX document includes completed truth table and a statement below the truth table which states if the statement is a tautology, contradiction or contingency.


Upon completion of the truth table, the user would need to tick the correct box to determine whether the propositional logic expression is a:
- Tautology (all the last column truth values are T)
- Contradiction (all the last column truth values are F)
- Contingency (contains T and F in the last column's truth values)


3. Merge option that generates two questions from the previous versions into one document (Version 3)


|        Question(s) Document           |     Answer(s) Document       |
|            :---:                      |           :---:              |       
|      LaTeX document merges the questions from version 1 and 2 into one document as long as their is more than one formula to be generated. This provides more content to test a user; to see if they have understood how to answer both type of questions. It also makes use of the random library which creates random empty boxes where the truth values would be for the intermediate and last column.          |    LaTeX document provides the solutions to both of questions similarly to the other types of answer documents.




LaTeX (.tex) files can be opened via the [Overleaf](https://www.overleaf.com/) website upon signing in or registering.


## License
The software is a free open source license under the terms of [GPL](https://www.gnu.org/licenses/gpl-3.0.en.html) version 3. The LICENSE document is also included within the repository.


## References/Acknowledgments
- [Propositional Logic Clause Parser](https://github.com/markomanninen/PLCParser/blob/master/pyPLCParser/PLCParser.py) by Marko Mannine.
- Propositional logic operator symbols from [Wikipedia](https://en.wikipedia.org/wiki/List_of_logic_symbols)
- LaTeX source code basics from [Overleaf](https://www.overleaf.com/learn/latex/Learn_LaTeX_in_30_minutes)  
- LaTeX Packages can be found at [LaTeX Packages](https://ctan.org/pkg/)
